# MPC Basics

This repository contains some basic Massively Parallel Computing (MPC) examples. These are based on the Code Vault's [Unix Threads in C](https://www.youtube.com/playlist?list=PLfqABt5AS4FmuQf70psXrsMLEDQXNkLq2) and [Unix Processes in C](https://www.youtube.com/playlist?list=PLfqABt5AS4FkW5mOn2Tn9ZZLLDwA3kZUY) tutorial playlists.

The aim is to study MPC concepts and implement them in:
 
- C (pthread, Unix forking primitives, Windows MPC primitives)
- Java
- C (standard threads)
- C++ 
- Go 
- Rust
- C++ async
- Zig

In the order of evolution of multithreading paradigms.

I hope to add functional languages once I engage with them through a separate project.
