//
// Created by vivraan on 17-10-2023.
// A silliness I have to use until C23 receives wide compiler support.
//

#ifndef MPCB__SILLY_C23_H_
#define MPCB__SILLY_C23_H_

#include <stdint.h>
#if (__STDC_VERSION__ < 202300L)
// Compilers still don't agree on bool, true, and false being keywords in C17.
// We need to define them ourselves.
# if !_STDBOOL_H
#  define _STDBOOL_H // NOLINT(*-reserved-identifier)
// Alignment satisfaction + speed (https://nullprogram.com/blog/2023/10/08/)
#  define bool int32_t
#  define true 1
#  define false 0
#  define __bool_true_false_are_defined 1 // NOLINT(*-reserved-identifier)
# endif
// The silliest way to define nullptr -- just gives me some highlighting in the text
# if nullptr
#  undef nullptr
# else
#  define nullptr nullptr
# endif
// Another silliness
# if constexpr
#  undef constexpr
# else
#  define constexpr constexpr
# endif
#else
#undef nullptr
#undef constexpr
#endif//__STDC_VERSION__ < 202300L

#endif//MPCB__SILLY_C23_H_
