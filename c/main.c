#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

#define NUM_PROCESSES 10

struct piep {
  int read_fd;
  int write_fd;
};

int main() {
  int pids[NUM_PROCESSES];
  struct piep pipes[NUM_PROCESSES + 1];
  for (int i = 0; i <= NUM_PROCESSES; i++) {
    int fds[2];
    if (pipe(fds) < 0) {
      perror("Couldn't create pipe");
      exit(1);
    }
    pipes[i].read_fd = fds[0];
    pipes[i].write_fd = fds[1];
  }

  for (int i = 0; i < NUM_PROCESSES; i++) {
    pids[i] = fork();
    if (pids[i] < 0) {
      perror("Couldn't fork");
      exit(1);
    } else if (pids[i] == 0) {
      // Child process
      for (int j = 0; j < NUM_PROCESSES; j++) {
        if (j != i) {
          close(pipes[j].read_fd);
        }
        if (j != i + 1) {
          close(pipes[j].write_fd);
        }
      }
      int x;
      if (read(pipes[i].read_fd, &x, sizeof(x)) < 0) {
        perror("Couldn't read");
        exit(1);
      }
      printf("[%d] y_%d = %d\n", getpid(), i, x);
      x++;
      if (write(pipes[i + 1].write_fd, &x, sizeof(x)) < 0) {
        perror("Couldn't write");
        exit(1);
      }
      printf("[%d] Sent %d\n", getpid(), x);
      close(pipes[i].read_fd);
      close(pipes[i + 1].write_fd);
      return 0;
    }
  }

  // Parent process
  for (int i = 0; i < NUM_PROCESSES; i++) {
    if (i != 0) {
      close(pipes[i].write_fd);
    }
    if (i != NUM_PROCESSES - 1) {
      close(pipes[i].read_fd);
    }
  }
  int y = 5;
  if (write(pipes[0].write_fd, &y, sizeof(y)) < 0) {
    perror("Couldn't write");
    exit(1);
  }
  printf("[%d] y_0 = %d\n", getpid(), y);
  close(pipes[0].write_fd);
  if (read(pipes[NUM_PROCESSES].read_fd, &y, sizeof(y)) < 0) {
    perror("Couldn't read");
    exit(1);
  }
  printf("[%d] y = %d\n", getpid(), y);
  close(pipes[NUM_PROCESSES].read_fd);

  return 0;
}