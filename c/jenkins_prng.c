//
// Created by vivraan on 15-10-2023.
//

#include "jenkins_prng.h"
#include <stddef.h>
#include <math.h>

static inline uint32_t rot(uint32_t x, uint32_t k) {
  return (x << k) | (x >> (32 - k));
}

void jenkins_prng_init_seed(struct jenkins_prng_state s[static 1], uint32_t seed) {
  s->a = 0xf1ea5eed;
  s->b = s->c = s->d = seed;

  for (size_t i = 0; i < 20; i++) {
    jenkins_prng_next_u32(s);
  }
}

void jenkins_prng_init_default(struct jenkins_prng_state state[static 1]) {
  jenkins_prng_init_seed(state, 0xdeadbeef);
}

uint32_t jenkins_prng_next_u32(struct jenkins_prng_state s[static 1]) {
  uint32_t e = s->a - rot(s->b, 27);
  s->a = s->b ^ rot(s->c, 17);
  s->b = s->c + s->d;
  s->c = s->d + e;
  s->d = e + s->a;
  return s->d;
}

int32_t jenkins_prng_next_i32(struct jenkins_prng_state state[static 1]) {
  union {
    uint32_t u32;
    int32_t i32;
  } var = {.u32 = jenkins_prng_next_u32(state)};
  return var.i32;
}

float jenkins_prng_next_f32(struct jenkins_prng_state state[static 1]) {
  union {
    uint32_t u32;
    float f32;
  } var;

  do {
    var.u32 = jenkins_prng_next_u32(state);
  } while (!isnormal(var.f32));

  return var.f32;
}
