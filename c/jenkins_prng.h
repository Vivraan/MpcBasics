//
// Created by vivraan on 15-10-2023.
// https://www.reddit.com/r/cpp/comments/8vhrzh/better_c_pseudo_random_number_generator/
//

#ifndef MPCB__JENKINS_PRNG_H_
#define MPCB__JENKINS_PRNG_H_

#include <stdint.h>

struct jenkins_prng_state {
  uint32_t a;
  uint32_t b;
  uint32_t c;
  uint32_t d;
};

void jenkins_prng_init_seed(struct jenkins_prng_state s[static 1], uint32_t seed);
void jenkins_prng_init_default(struct jenkins_prng_state state[static 1]);

#define JENKINS_PRNG_INIT_FUNC_OVERLOAD_INVOKE(_1, _2, NAME, ...) NAME
#define jenkins_prng_init(...)                                                                             \
  JENKINS_PRNG_INIT_FUNC_OVERLOAD_INVOKE(__VA_ARGS__, jenkins_prng_init_seed, jenkins_prng_init_default, ) \
  (__VA_ARGS__)

uint32_t jenkins_prng_next_u32(struct jenkins_prng_state s[static 1]);
int32_t jenkins_prng_next_i32(struct jenkins_prng_state state[static 1]);
float jenkins_prng_next_f32(struct jenkins_prng_state state[static 1]);

#endif//MPCB__JENKINS_PRNG_H_
